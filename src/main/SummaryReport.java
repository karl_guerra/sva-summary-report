package main;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SummaryReport {

	public static void main(String[] args) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");  
        Date date = new Date();
        String currDate = formatter.format(date);
        System.out.println("Current Date: "+currDate);
		
//		String username = "ADMDBMCJ", password = "hyZm68NG2xmhfnNY";
        String username = "MSENTESRVC", password = "zLKU9pWDQs";
		System.out.println("GETTING MSISDN & BALANCE FROM TBLCURRENTSTOCK, Please Wait....");
		System.out.println("");
		
		try {
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection(  
					"jdbc:oracle:thin:@172.18.3.6:1521/MMPRODDB"
					+ "",username,password);
			
			System.out.println("--------------------------------------");
	        System.out.println("| Getting Records FROM CURRENTSTOCK! |");
	        System.out.println("--------------------------------------");
	        
	        String st = "select inf.TYPE, SUM(ft.TBALANCE) AS TOTAL  FROM \r\n" + 
	        		"(\r\n" + 
	        		"   SELECT DISTINCT To_CHAR(CBALANCE.MSISDN) AS MSISDN, TBALANCE  FROM\r\n" + 
	        		"   (\r\n" + 
	        		"      SELECT MSISDN, Decrypt(AMOUNT, 'FhTQJUjwC4BmLBYwqKm', MSISDN)/100 AS CBALANCE, WALLETID\r\n" + 
	        		"      FROM TBLCURRENTSTOCK WHERE WALLETID = 0 AND MSISDN > 0 \r\n" + 
	        		"      AND MSISDN IN (SELECT DISTINCT a.MSISDN FROM TBLTRANSACTIONDETAILS a)\r\n" + 
	        		"   ) CBALANCE \r\n" + 
	        		"   LEFT JOIN\r\n" + 
	        		"   (SELECT MSISDN, BALANCEAFTER/100 AS TBALANCE FROM TBLTRANSACTIONDETAILS) TBALANCE \r\n" + 
	        		"   ON CBALANCE.MSISDN = TBALANCE.MSISDN AND CBALANCE.CBALANCE = TBALANCE.TBALANCE\r\n" + 
	        		"   UNION\r\n" + 
	        		"   SELECT TO_CHAR(c.MSISDN), Decrypt(c.AMOUNT, 'FhTQJUjwC4BmLBYwqKm', c.MSISDN)/100 FROM TBLCURRENTSTOCK c WHERE c.MSISDN > 0\r\n" + 
	        		"   and c.WALLETID = 0 and c.MSISDN NOT IN (SELECT DISTINCT td.MSISDN FROM TBLTRANSACTIONDETAILS td)\r\n" + 
	        		") ft\r\n" + 
	        		"INNER JOIN\r\n" + 
	        		"(\r\n" + 
	        		"    SELECT MSISDN, Decrypt(TYPE, 'FhTQJUjwC4BmLBYwqKm', MSISDN) AS TYPE FROM TBLMOBILEACCOUNTINFO\r\n" + 
	        		") \r\n" + 
	        		"INF ON ft.MSISDN = INF.MSISDN GROUP BY inf.TYPE";
			Statement stmt=con.createStatement();  
			ResultSet rs = stmt.executeQuery(st);
			
			List<List<String>> rows = new ArrayList<>();
			while(rs.next()) {
				
				String amnt = rs.getString(2), 
					amount = "";
				if(amnt == null) {
					amount = "0";
				}
				else {
					amount = amnt;
				}
				System.out.println(rs.getString(1)+" "+rs.getString(2));
				String[] ar = {rs.getString(1),rs.getString(2)};
				List<String> af = Arrays.asList(ar);
				rows.add(af);
			}
			con.close();
			System.out.println("----------------------------------------------------------------------");
			System.out.println("| Now Exporting 'SVA Summary Report "+ currDate +".xslx' file.... |");
			System.out.println("----------------------------------------------------------------------");
			
			Export(rows, currDate);
			
			System.out.println("");
			System.out.println("---------------------");
			System.out.println("| Status: Finished! |");
			System.out.println("---------------------");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	public static void Export(List<List<String>> rows, String currDate) {
		
		//Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();
        
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee Data");
        
        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        data.put("1", new Object[] {"ACCOUNT TYPE", "TOTAL"});
        int ctr = 2;
        for (List<String> rowData : rows) {
		    
        	data.put(""+ctr, new Object[] {rowData.get(0),rowData.get(1)});
        	ctr++;
        }
        
        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        
        try
        {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File("SVA Summary Report "+currDate+".xlsx"));
            workbook.write(out);
            out.close();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
	}
}
